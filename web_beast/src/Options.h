#pragma once

#include <boost/program_options.hpp>

#include <util/Singleton.h>

#include <fstream>

namespace Webserver
{
    class Options : public Util::SingletonExtInit<Options>
    {
        boost::program_options::variables_map vm;
        boost::program_options::options_description desc;

        public:
            Options(int argc, char* argv[]);

            template<typename T>
            static auto& Value(std::string name)
            {
                return Instance()->vm[name].as<T>();
            }

            static bool IsSet(std::string name);

            static bool HandleHelp();
    };
}
