#pragma once

namespace Webserver {
	namespace Util
	{
		template<typename T>
		class Singleton {
		public:
			static T* Instance()
			{
				static T* myInstance = nullptr;
				if (myInstance == nullptr) {
					myInstance = new T;
				}
				return myInstance;
			}
		};


		template<typename T>
		class SingletonExtInit {
		public:
			static T* Instance()
			{
				return MyInstance(nullptr);
			}

			static void Init(T* inst)
			{
				MyInstance(inst);
			}

			static void Reset()
			{
				MyInstance(nullptr, true);
			}

		private:
			static T* MyInstance(T* inst, bool acceptNull = false)
			{
				static T* myInstance = nullptr;
				if (inst != nullptr || acceptNull) {
					myInstance = inst;
				}
				return myInstance;
			}

		};
	}
}
