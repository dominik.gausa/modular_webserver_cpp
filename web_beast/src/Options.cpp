#include "Options.h"
#include <iostream>

namespace po = boost::program_options;

namespace Webserver
{
  Options::Options(int argc, char* argv[]) :
    desc("Allowed options")
  {
    desc.add_options()
      ("help,?",      "Help")
      ("host,h",      po::value<std::string>()->default_value("0.0.0.0"), "Host to bind")
      ("port,p",      po::value<int>()->default_value(8080),              "Port")
      ("dir,d",       po::value<std::string>()->default_value("."),       "Dir")
      ("threads,t",   po::value<int>()->default_value(1),                 "Workerthreads")
      ("motd,m",      po::value<std::string>()->default_value("motd"),    "Message of the Day")
      ;
    
    std::ifstream configFile("config.ini");
    po::store(po::parse_command_line(argc, argv, desc), vm);
    po::store(po::parse_config_file(configFile, desc), vm);
    po::notify(vm);
  }

  bool Options::IsSet(std::string name)
  {
    return Instance()->vm.count(name);
  }

  bool Options::HandleHelp()
  {
    if (Options::IsSet("help")) {
      std::cout << Instance()->desc << "\n";
      return true;
    }
    return false;
  }
}