#include "api/APIBase.h"


namespace Webserver
{

	class APIDummy : public APIBase
	{
		std::string text;
	public:
		APIDummy(std::string text) : text(text)
		{
		}


		virtual bool DoExecute(std::string call, std::string reqBody, std::stringstream& out) override
		{
			out << "APIDummy: " << text;
			return true;
		}
	};

	class APIDummyRouter : public APIRouter
	{
		APIDummy		dummya;
		APIDummy		dummyb;

		public:
			APIDummyRouter() :
				dummya("A"),
				dummyb("B")
			{
				Register("/a", &dummya);
				Register("/b", &dummyb);
				API::Instance()->Register("/dummy", this);
			}
	};
}

static Webserver::APIDummyRouter dummy;