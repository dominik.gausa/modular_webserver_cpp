#pragma once

#include "util/Singleton.h"

#include <functional>
#include <sstream>
#include <string>
#include <map>

namespace Webserver {
	class APIBase
	{

	public:
		APIBase();

		virtual bool Execute(std::string call, std::string reqBody, std::stringstream& out) final;
		virtual bool DoExecute(std::string call, std::string reqBody, std::stringstream& out);
	};

	class APIRouter : public APIBase
	{
	protected:
		std::map<std::string, APIBase*> list;

	public:
		APIRouter() {}

		virtual void Register(std::string call, APIBase* api) final;
		virtual bool DoExecute(std::string call, std::string reqBody, std::stringstream& out) override;
	};

	class API : public APIRouter, public Util::Singleton<API>
	{
	public:
		API() {}
	};
}

