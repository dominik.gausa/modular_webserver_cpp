#include "api/APIBase.h"


#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/json_parser.hpp>

namespace Webserver
{
	class TestJSON : public APIBase
	{
		static unsigned int call;
	public:
		TestJSON()
		{
			API::Instance()->Register("/json", this);
		}


		virtual bool DoExecute(std::string /* call */, std::string reqBody, std::stringstream& bodyOut) override
		{
			namespace pt = boost::property_tree;
			bodyOut << "API Base<br>Req: '" << reqBody << "'";
			bodyOut << "<br>";

			try {
				std::stringstream bodyInStream;
				bodyInStream << reqBody;
				pt::ptree root;
				pt::read_json(bodyInStream, root);
				bodyOut << "value1: " << root.get<int>("value1");
				bodyOut << "<br>";

				for (pt::ptree::value_type& row : root.get_child("value3")) {
					bodyOut << " ->" << row.second.get_value<int>() << "<br>";
				}
			}
			catch (...) {}

			try {
				boost::property_tree::ptree root;

				pt::ptree fruits_node;
				for (auto& val : std::vector<std::string>{ "banana", "apple" })
				{
					pt::ptree fruit;
					fruit.put("", val);
					fruits_node.push_back(std::make_pair("", fruit));
				}
				root.add_child("Array", fruits_node);

				root.put("call", call);

				boost::property_tree::write_json(bodyOut, root);

				call++;
			}
			catch (...) {}

			return true;
		}
	};

}

unsigned int Webserver::TestJSON::call = 0U;
static Webserver::TestJSON json;
