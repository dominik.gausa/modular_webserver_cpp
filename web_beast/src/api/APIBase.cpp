#include "api/APIBase.h"

#include <iostream>
#include <map>


namespace Webserver
{
	APIBase::APIBase() {}

	bool APIBase::Execute(std::string call, std::string reqBody, std::stringstream& out)
	{
		return DoExecute(call, reqBody, out);
	}

	bool APIBase::DoExecute(std::string call, std::string reqBody, std::stringstream& out)
	{
		out << "not implemented" << std::endl;
		return true;
	}


	bool APIRouter::DoExecute(std::string call, std::string reqBody, std::stringstream& out)
	{
		std::cout << "API Search for " << call << std::endl;
		std::string dir = call;
		std::size_t pos = dir.find_first_of('/', 1);
		if(pos >= 1)
			dir = dir.substr(0, pos);

		auto handler = list[dir];
		if (!handler){
			std::cout << "No Handler found " << std::endl;
			return false;
		}
		
		handler->Execute(call.substr(dir.length()), reqBody, out);
		return true;
	}

	void APIRouter::Register(std::string call, APIBase* api)
	{
		list.emplace(std::make_pair(call, api));
	}
}